import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { ScrollView, StatusBar } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

import { ListItem, Separator } from '../components/List';

const styles = EStyleSheet.create({
  $green: '$primaryGreen',
  $pink: '$primaryPink',
  $purple: '$primaryPurple',
  $aqua: '$primaryAqua',
  $orange: '$primaryOrange',
  $coal: '$primaryCoal',
});

class Themes extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

    handleThemePress = (color) => {
      const { navigation } = this.props;
      navigation.navigate('Home');
      // navigation.goBack();

      console.log('press theme', color);
    };

    render() {
      return (
        <ScrollView>
          <StatusBar translucent={false} barStyle="default" />

          <ListItem
            text="Green"
            onPress={() => this.handleThemePress(styles.$green)}
            selected
            checkmark={false}
            iconBackground={styles.$green}
          />
          <Separator />

          <ListItem
            text="Pink"
            onPress={() => this.handleThemePress(styles.$pink)}
            selected
            checkmark={false}
            iconBackground={styles.$pink}
          />
          <Separator />

          <ListItem
            text="Purple"
            onPress={() => this.handleThemePress(styles.$purple)}
            selected
            checkmark={false}
            iconBackground={styles.$purple}
          />
          <Separator />

          <ListItem
            text="Aqua"
            onPress={() => this.handleThemePress(styles.$aqua)}
            selected
            checkmark={false}
            iconBackground={styles.$aqua}
          />
          <Separator />

          <ListItem
            text="Orange"
            onPress={() => this.handleThemePress(styles.$orange)}
            selected
            checkmark={false}
            iconBackground={styles.$orange}
          />
          <Separator />

          <ListItem
            text="Coal"
            onPress={() => this.handleThemePress(styles.$coal)}
            selected
            checkmark={false}
            iconBackground={styles.$coal}
          />
          <Separator />

        </ScrollView>
      );
    }
}

export default Themes;
