import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Provider } from 'react-redux';

import Navigator from './config/routes';
import { AlertProvider } from './components/Alert';
import store from './config/store';

// delete later
// import './reducers';

EStyleSheet.build({
  $primaryGreen: '#cbf442',
  $primaryPink: '#b317b3',
  $primaryPurple: '#7930cb',
  $primaryAqua: '#2f8695',
  $primaryOrange: '#c07530',
  $primaryCoal: '#252525',

  $white: '#FFFF',
  $borderColor: '#E2E2E2',
  $inputText: '#797979',
  $lightGreen: '#ABE999',
  $darkText: '#343434',
  $black: '#000000',
});

export default () => (
  <Provider store={store}>
    <AlertProvider>
      <Navigator onNavigationStateChange={null} />
    </AlertProvider>
  </Provider>
);
