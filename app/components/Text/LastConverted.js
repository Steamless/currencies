import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';
import moment from 'moment';

import styles from './styles';

const LastConverted = ({
  base, quote, converionRate, date,
}) => (
  <Text style={styles.smallText}>


  1
    {' '}
    {base}
    {' '}

=
    {' '}
    {converionRate}
    {' '}
    {' '}
    {quote}
    {' '}

as of
    {' '}
    {moment(date).format('D MMM YYYY')}
  </Text>
);

LastConverted.propTypes = {
  date: PropTypes.object,
  base: PropTypes.string,
  quote: PropTypes.string,
  converionRate: PropTypes.number,
};

export default LastConverted;
