import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  smallText: {
    color: '$white',
    fontSize: 20,
    textAlign: 'center',
  },
});

export default styles;
