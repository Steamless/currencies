import EStyleSheet from 'react-native-extended-stylesheet';
import { StyleSheet } from 'react-native';

const styles = EStyleSheet.create({
  $underlayColor: '$borderColor',
  row: {
    paddingHorizontal: 20,
    paddingVertical: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '$white',
  },

  text: {
    fontSize: 16,
    color: '$darkText',
  },

  separator: {
    marginLeft: 20,
    backgroundColor: '$borderColor',
    flex: 1,
    height: StyleSheet.hairLineWidth,
  },

  icon: {
    backgroundColor: 'transparent',
    borderRadius: 15,
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },

  iconVisible: {
    backgroundColor: '$black',
  },

  checkIcon: {
    width: 18,
  },

});

export default styles;
